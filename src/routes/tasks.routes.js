import { Router } from 'express';
import { connectClient } from '../database';
import { ObjectID } from 'mongodb';
import { sendEmailNotification } from '../models/email';

const router = Router();



// Get Users By status
router.get('/:status', async (req,res) => {
  const { status } = req.params;
  const db = await connectClient();
  console.log("Status Usuario: "+status);
  const result = await db.collection('usuarios').find({status: status}).toArray();
  console.log(result);
  res.json(result);
});

//Get All Users
router.get('/', async (req,res) => {
  const db = await connectClient();
  const result = await db.collection('usuarios').find({}).toArray();
  console.log(result);
  res.json(result);
});

//Get All Users Pagin
router.get('/page/:page', async (req,res) => {
  const db = await connectClient();
  var { page } = req.params;
  var limit=3;
  if(page==1){
    page=0;
  }else{
    page=(page-1)*limit;
  }
  console.log("Page: "+page);
  const result = await db.collection('usuarios').find({}).sort({_id:1}).limit(limit).skip(page).toArray();
  console.log(result);
  res.json(result);
});

//Get User by ID
router.get('/:id', async (req,res) => {
  const { id } = req.params;
  const db = await connectClient();
  console.log("id: "+id);
  const result = await db.collection('usuarios').findOne({ _id: ObjectID(id)});
  res.json(result);

});

//Delete User
router.delete('/:id', async (req,res) => {
  const { id } = req.params;
  const db = await connectClient();
  const registro = await db.collection('usuarios').findOne({ _id: ObjectID(id)});
  const result = await db.collection('usuarios').deleteOne({ _id: ObjectID(id)});
  res.json({
    message: 'Usuario '+registro.username+' eliminado',
    result
  });

});

//Update User
router.put('/:id', async (req,res) => {
  console.log("Actualizand Usuario ....")
  const { id } = req.params;
  const db = await connectClient();
  const updateInput = {
    username: req.body.username,
    password: req.body.password,
    rol: req.body.rol,
    status: req.body.status,
    nameUser: req.body.nameUser,
    email: req.body.email
  };
  sendEmailNotification(req.body.email);
  const result = await db.collection('usuarios').updateOne({ _id: ObjectID(id)}, { $set: updateInput});
  res.json({
    message: 'Usuario '+req.body.username+' actualizado',
  });
  console.log("..... Finalizando actualización de Usuario")
});




export default router;
