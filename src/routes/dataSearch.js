import { Router } from 'express';
import { connectData, connectMenu, connectAccount } from '../database';

const router = Router();
const limitPageSize = 11;

router.get('/Pagination/:page', async (req, res) => {
  const db = await connectData();
  var { page } = req.params;

  var rowsSkip;
  if(page==1){
    rowsSkip=0;
  }else{
    rowsSkip=(page-1)*limitPageSize;
  }
  console.log("Page: "+page+" Skip: "+rowsSkip);
  const result = await db.collection('datos').find({}).sort({_id:1}).limit(limitPageSize).skip(rowsSkip).toArray();
  var registros = await db.collection('datos').countDocuments({});
  registros = registros-rowsSkip;
  if(registros<=limitPageSize){
    registros=0;
  }
  console.log(result.length);
  res.status(200).json({"table":result,"restante":registros});
});

router.post('/byFilter/:page', async (req, res) => {
  console.log("Searching byFilter.....");
  const db = await connectData();
  var result;
  var registros;
  var { page } = req.params;
  const {typeRequest,cuenta,emisor,serie,mercado} = req.body;

  var rowsSkip;
  if(page==1){
    rowsSkip=0;
  }else{
    rowsSkip=(page-1)*limitPageSize;
  }
  console.log("page:"+page+"rowsSkip: "+rowsSkip+" typeRequest: "+typeRequest+" cuenta: "+cuenta+" emisor: "+emisor+" serie: "+serie+" mercado: "+mercado);
  switch (typeRequest) {
    case 0:
      result = await db.collection('datos').find().sort({CONSECUTIVO:1}).limit(limitPageSize).skip(rowsSkip).toArray();
      registros = await db.collection('datos').countDocuments();
      break;
    case 2:
    case 1:
      result = await db.collection('datos').find({NU_CUENTA:cuenta}).sort({CONSECUTIVO:1}).limit(limitPageSize).skip(rowsSkip).toArray();
      registros = await db.collection('datos').countDocuments({NU_CUENTA:cuenta});
      break;
    case 2:
      result = await db.collection('datos').find({"NU_CUENTA":cuenta,"CD_EMISOR_APLICA":emisor}).sort({CONSECUTIVO:1}).limit(limitPageSize).skip(rowsSkip).toArray();
      registros = await db.collection('datos').countDocuments({"NU_CUENTA":cuenta,"CD_EMISOR_APLICA":emisor});
      break;
    case 3:
      result = await db.collection('datos').find({"NU_CUENTA":cuenta,"CD_EMISOR_APLICA":emisor,"CD_SERIE_APLICA":serie}).sort({CONSECUTIVO:1}).limit(limitPageSize).skip(rowsSkip).toArray();
      registros = await db.collection('datos').countDocuments({"NU_CUENTA":cuenta,"CD_EMISOR_APLICA":emisor,"CD_SERIE_APLICA":serie});
      break;

    default:

  }
  registros = registros-rowsSkip;
  if(registros<=limitPageSize){
    registros=0;
  }
  console.log("Registros restantes: "+registros);
  console.log(".....Searching byFilter END");
  res.status(200).json({"table":result,"restante":registros});
});

router.post('/filtro', async (req, res) =>{
  const db = await connectData();
  var datos = db.collection('datos');
  const {typeRequest,cuenta,emisor,serie,mercado} = req.body;
  var result;
  switch (typeRequest) {
    case 0:
      result = await db.collection('datos').distinct("NU_CUENTA");
      break;
    case 1:
      result = await db.collection('datos').distinct("CD_EMISOR_APLICA", {"NU_CUENTA":cuenta});
      break;
    case 2:
      result = await db.collection('datos').distinct("CD_SERIE_APLICA", {"NU_CUENTA":cuenta,"CD_EMISOR_APLICA":emisor});
      break;
    case 3:
      result = await db.collection('datos').distinct("CD_MERCADO",{"NU_CUENTA":cuenta,"CD_EMISOR_APLICA":emisor,"CD_SERIE_APLICA":serie});
      break;

    default:

  }
  console.log(JSON.stringify(result));
  res.status(200).json(result);

});
/*
router.get('/menus', async (req, res) => {
  const { tipo } = req.body;
  const db = await connectMenu();
  var result;
  console.log("Typo de Menu 1: "+tipo);
  if(tipo=="rol"){
      console.log("1");
    result = await db.collection('menu').find({"tipo":"rol"}).toArray();
  } else {
    console.log("2");
    result = await db.collection('menu').find({"tipo":"estatus"}).toArray();
  }
  console.log(result);
  res.status(200).json(result);
});
*/
router.get('/:tipo', async (req, res) => {
  console.info("Get menus start....");
  const { tipo } = req.params;
  const db = await connectMenu();
  var result;
  console.log("Typo de Menu: "+tipo);
  switch (tipo) {
    case "rol":
      console.debug(tipo);
      result = await db.collection('menu').find({"tipo":"rol"}).toArray();
      break;
    case "estatus":
      console.debug(tipo);
      result = await db.collection('menu').find({"tipo":"estatus"}).toArray();
      break;
    default:
      result={};
      res.send(204,result);
  }
  console.info(" ....Get menus end");
  res.status(200).send(result);
});

router.get('/account/:page', async (req, res) => {
  const db = await connectAccount();
  var { page } = req.params;

  var rowsSkip;
  if(page==1){
    rowsSkip=0;
  }else{
    rowsSkip=(page-1)*limitPageSize;
  }
  console.log("Page: "+page+" Skip: "+rowsSkip);
  const result = await db.collection('cuentas').find({}).sort({_id:1}).limit(limitPageSize).skip(rowsSkip).toArray();
  var registros = await db.collection('cuentas').countDocuments({});
  registros = registros-rowsSkip;
  if(registros<=limitPageSize){
    registros=0;
  }
  res.status(200).json({"table":result,"restante":registros});
});




export default router;
