import { Router } from 'express';
import { connectClient } from '../database';
import { sendEmailRegistry } from '../models/email';

const router = Router();
const config = require('../config');

// Log In user, and validate if is ACTIVE or NOT
router.post('/logIn', async (req, res, next) => {
  const db = await connectClient();
  const { username, password } = req.body;
  const result = await db.collection('usuarios').findOne({username:username});
  if(!result){
    console.log("Usuario no valido");
    return res.status(404).send("Usuario no valido: "+ result);
  }else if(password!=result.password){
    console.log("Contraseña no valido - INPUT: "+password+" BD: "+result.password);
    return res.status(404).send("Contraseña no valido");
  }else if(result.estatus=="DISABLE"){
    console.log("Usuario desactivado");
    return res.status(403).send("Usuario sin permisos");
  }

  res.status(202).json({auth:true, rol:result.rol});
});


//Create New User (Need Approve)
router.post('/signUp', async (req,res) => {
  console.log("Creando registro ....");
  const db = await connectClient();
  const insertInput = {
    username: req.body.user,
    password: req.body.password,
    status: req.body.status,
    nameUser: req.body.userName,
    email: req.body.email
  };
  const existUser = await db.collection('usuarios').find({username:req.body.user}).count();
  console.log(existUser);
  if(existUser>0){
    console.log(".... Registro Existente");
    res.status(409).send("Usuario existente");
  }else{
    sendEmailRegistry(insertInput);
    const result = await db.collection('usuarios').insertOne(insertInput);
    console.log(".... Registro creado");
    res.status(201).json(insertInput);
  }

});



export default router;
