import MongoClient from 'mongodb';
import mongoose from 'mongoose';
require('dotenv').config();

const mongoURI=process.env.MONGODB_URI;
const usuariosBD = process.env.USUARIOS_DB;
const movimientosDB = process.env.MOVIMIENTOS_DB;
const menusBD = process.env.MENUS_DB;
const cuentasDB = process.env.CUENTAS_DB;

export async function connectClient() {
  try{
    const client = await MongoClient.connect(mongoURI, {
      useUnifiedTopology: true
    });
    const db = client.db(usuariosBD);
    console.log('Success DB connect');
    return db;
  }catch(e){
    console.log(e);
  }
}

export async function connectData() {
  try{
    const client = await MongoClient.connect(mongoURI, {
      useUnifiedTopology: true
    });
    const db = client.db(movimientosDB);
    console.log('Success DB connect');
    return db;
  }catch(e){
    console.log(e);
  }
}

export async function connectMenu() {
  try{
    const client = await MongoClient.connect(mongoURI, {
      useUnifiedTopology: true
    });
    const db = client.db(menusBD);
    console.log('Success DB connect');
    return db;
  }catch(e){
    console.log(e);
  }
}

  export async function connectAccount() {
    try{
      const client = await MongoClient.connect(mongoURI, {
        useUnifiedTopology: true
      });
      const db = client.db(cuentasDB);
      console.log('Success DB connect');
      return db;
    }catch(e){
      console.log(e);
    }
}
