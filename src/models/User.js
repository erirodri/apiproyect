const { Schema, model }  = require('mongoose');
const bcrypt = require('bcrypt');

const userSchema = new Schema({
  username: String,
  email: String,
  password: String
});

userSchema.methods.encryptPassword = async (password) => {
  const encript = await bcrypt.genSalt(10);
  return bcrypt.hash(password, encript);
};
module.exports = model('User', userSchema);
