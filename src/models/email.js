import nodemailer from 'nodemailer';

const smtp_serv=process.env.SMTP_SERVICE;
const smtp_user=process.env.SMTP_USER;
const smtp_pass=process.env.SMTP_PASSW;
const smtp_from=process.env.SMTP_FROM;

const emailTransport = nodemailer.createTransport({
  service: smtp_serv,
  auth: {
    user: smtp_user,
    pass: smtp_pass,
    port: 465,
    secure: true,
    tls: {
      minVersion: 'TLSv1.2',
      ciphers: 'TLS_AES_128_GCM_SHA256',
    }
  }
});
export function sendEmailRegistry(inputValues){
  const mailBody = {
    from:'erkrodriguez.m@gmail.com',
    to: 'erick.rodriguez.morales@bbva.com',
    subject: '[SIVA_MUV_APP] Nuevo Registro',
    html:'<h1> Validacion nuevo registro</h1>'+
      '<h2>Es necesaria su aprobacion del siguiente registro para permitir el acceso al usuario solicitante:</h2>'+
      '<p>Nombre: '+inputValues.nameUser+'</p>'+
      '<p>Usuario (M): '+inputValues.username+'</p>'+
      '<p>Correo: '+inputValues.email+'</p>'
  };
  emailTransport.sendMail(mailBody, (err,info) => {
    if(err){
      console.log("ERROR al enviar correo: "+err.message);
      res.send(500, err.message);
    }else{
      console.log("Correo enviado correctamente"+ info);
    }
  });
}

export function sendEmailNotification(email){
  const mailBody = {
    from:'erkrodriguez.m@gmail.com',
    to: email,
    subject: '[SIVA_MUV_APP] Registro Exitoso',
    html:'<h1> Registro completado </h1>'+'<h2>Su registro a sido confirmado:</h2>'+
      '<p>Ya puedes iniciar sesión con tus datos proporcionados en tu registro.</p>'
  };
  emailTransport.sendMail(mailBody, (err,info) => {
    if(err){
      console.log("ERROR al enviar correo: "+err.message);
      res.send(500, err.message);
    }else{
      console.log("Correo enviado correctamente"+ info.message);
    }
  });
}
