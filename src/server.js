require('dotenv').config();
const express = require('express');
const app = express();


//Routes
import indexRoutes from './routes/index.routes';
import TaskRoutes from './routes/tasks.routes';
import authUserController from './routes/authUserController';
import DataSearch from './routes/dataSearch';

// Settings
app.set('port', 3000);

//Middleware
app.use(express.json());
app.use(express.urlencoded({extended: false}));

//Routes
app.use(indexRoutes);
app.use('/users', TaskRoutes);
app.use(authUserController);
app.use('/data',DataSearch);

export default app;
