"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _index = _interopRequireDefault(require("./routes/index.routes"));

var _tasks = _interopRequireDefault(require("./routes/tasks.routes"));

var _authUserController = _interopRequireDefault(require("./routes/authUserController"));

var _dataSearch = _interopRequireDefault(require("./routes/dataSearch"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

require('dotenv').config();

var express = require('express');

var app = express(); //Routes

// Settings
app.set('port', 3000); //Middleware

app.use(express.json());
app.use(express.urlencoded({
  extended: false
})); //Routes

app.use(_index["default"]);
app.use('/users', _tasks["default"]);
app.use(_authUserController["default"]);
app.use('/data', _dataSearch["default"]);
var _default = app;
exports["default"] = _default;