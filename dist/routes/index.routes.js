"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var xlsxtojson = require("xlsx-to-json");

var router = (0, _express.Router)(); // Routes

router.get('/ss', function (req, res) {
  res.send('API PROYECT-PRACTITIONER');
});
router.post('/tools/excelToJson', function (req, res) {
  xlsxtojson({
    input: "/home/alumno/apiProyect/src/routes/CUENTAS_MD_ERICK.xlsx",
    output: "outputCuentas.json",
    lowerCaseHeader: true
  }, function (err, result) {
    if (err) {
      res.json(err);
    } else {
      res.json(result);
    }
  });
});
router.get('/tools', function (req, res) {
  res.send("HELLO Tools");
});
var _default = router;
exports["default"] = _default;