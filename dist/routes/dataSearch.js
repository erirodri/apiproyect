"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _database = require("../database");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var router = (0, _express.Router)();
var limitPageSize = 11;
router.get('/Pagination/:page', /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var db, page, rowsSkip, result, registros;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return (0, _database.connectData)();

          case 2:
            db = _context.sent;
            page = req.params.page;

            if (page == 1) {
              rowsSkip = 0;
            } else {
              rowsSkip = (page - 1) * limitPageSize;
            }

            console.log("Page: " + page + " Skip: " + rowsSkip);
            _context.next = 8;
            return db.collection('datos').find({}).sort({
              _id: 1
            }).limit(limitPageSize).skip(rowsSkip).toArray();

          case 8:
            result = _context.sent;
            _context.next = 11;
            return db.collection('datos').countDocuments({});

          case 11:
            registros = _context.sent;
            registros = registros - rowsSkip;

            if (registros <= limitPageSize) {
              registros = 0;
            }

            console.log(result.length);
            res.status(200).json({
              "table": result,
              "restante": registros
            });

          case 16:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}());
router.post('/byFilter/:page', /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var db, result, registros, page, _req$body, typeRequest, cuenta, emisor, serie, mercado, rowsSkip;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            console.log("Searching byFilter.....");
            _context2.next = 3;
            return (0, _database.connectData)();

          case 3:
            db = _context2.sent;
            page = req.params.page;
            _req$body = req.body, typeRequest = _req$body.typeRequest, cuenta = _req$body.cuenta, emisor = _req$body.emisor, serie = _req$body.serie, mercado = _req$body.mercado;

            if (page == 1) {
              rowsSkip = 0;
            } else {
              rowsSkip = (page - 1) * limitPageSize;
            }

            console.log("page:" + page + "rowsSkip: " + rowsSkip + " typeRequest: " + typeRequest + " cuenta: " + cuenta + " emisor: " + emisor + " serie: " + serie + " mercado: " + mercado);
            _context2.t0 = typeRequest;
            _context2.next = _context2.t0 === 0 ? 11 : _context2.t0 === 2 ? 18 : _context2.t0 === 1 ? 18 : _context2.t0 === 2 ? 25 : _context2.t0 === 3 ? 32 : 39;
            break;

          case 11:
            _context2.next = 13;
            return db.collection('datos').find().sort({
              CONSECUTIVO: 1
            }).limit(limitPageSize).skip(rowsSkip).toArray();

          case 13:
            result = _context2.sent;
            _context2.next = 16;
            return db.collection('datos').countDocuments();

          case 16:
            registros = _context2.sent;
            return _context2.abrupt("break", 39);

          case 18:
            _context2.next = 20;
            return db.collection('datos').find({
              NU_CUENTA: cuenta
            }).sort({
              CONSECUTIVO: 1
            }).limit(limitPageSize).skip(rowsSkip).toArray();

          case 20:
            result = _context2.sent;
            _context2.next = 23;
            return db.collection('datos').countDocuments({
              NU_CUENTA: cuenta
            });

          case 23:
            registros = _context2.sent;
            return _context2.abrupt("break", 39);

          case 25:
            _context2.next = 27;
            return db.collection('datos').find({
              "NU_CUENTA": cuenta,
              "CD_EMISOR_APLICA": emisor
            }).sort({
              CONSECUTIVO: 1
            }).limit(limitPageSize).skip(rowsSkip).toArray();

          case 27:
            result = _context2.sent;
            _context2.next = 30;
            return db.collection('datos').countDocuments({
              "NU_CUENTA": cuenta,
              "CD_EMISOR_APLICA": emisor
            });

          case 30:
            registros = _context2.sent;
            return _context2.abrupt("break", 39);

          case 32:
            _context2.next = 34;
            return db.collection('datos').find({
              "NU_CUENTA": cuenta,
              "CD_EMISOR_APLICA": emisor,
              "CD_SERIE_APLICA": serie
            }).sort({
              CONSECUTIVO: 1
            }).limit(limitPageSize).skip(rowsSkip).toArray();

          case 34:
            result = _context2.sent;
            _context2.next = 37;
            return db.collection('datos').countDocuments({
              "NU_CUENTA": cuenta,
              "CD_EMISOR_APLICA": emisor,
              "CD_SERIE_APLICA": serie
            });

          case 37:
            registros = _context2.sent;
            return _context2.abrupt("break", 39);

          case 39:
            registros = registros - rowsSkip;

            if (registros <= limitPageSize) {
              registros = 0;
            }

            console.log("Registros restantes: " + registros);
            console.log(".....Searching byFilter END");
            res.status(200).json({
              "table": result,
              "restante": registros
            });

          case 44:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function (_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}());
router.post('/filtro', /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var db, datos, _req$body2, typeRequest, cuenta, emisor, serie, mercado, result;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return (0, _database.connectData)();

          case 2:
            db = _context3.sent;
            datos = db.collection('datos');
            _req$body2 = req.body, typeRequest = _req$body2.typeRequest, cuenta = _req$body2.cuenta, emisor = _req$body2.emisor, serie = _req$body2.serie, mercado = _req$body2.mercado;
            _context3.t0 = typeRequest;
            _context3.next = _context3.t0 === 0 ? 8 : _context3.t0 === 1 ? 12 : _context3.t0 === 2 ? 16 : _context3.t0 === 3 ? 20 : 24;
            break;

          case 8:
            _context3.next = 10;
            return db.collection('datos').distinct("NU_CUENTA");

          case 10:
            result = _context3.sent;
            return _context3.abrupt("break", 24);

          case 12:
            _context3.next = 14;
            return db.collection('datos').distinct("CD_EMISOR_APLICA", {
              "NU_CUENTA": cuenta
            });

          case 14:
            result = _context3.sent;
            return _context3.abrupt("break", 24);

          case 16:
            _context3.next = 18;
            return db.collection('datos').distinct("CD_SERIE_APLICA", {
              "NU_CUENTA": cuenta,
              "CD_EMISOR_APLICA": emisor
            });

          case 18:
            result = _context3.sent;
            return _context3.abrupt("break", 24);

          case 20:
            _context3.next = 22;
            return db.collection('datos').distinct("CD_MERCADO", {
              "NU_CUENTA": cuenta,
              "CD_EMISOR_APLICA": emisor,
              "CD_SERIE_APLICA": serie
            });

          case 22:
            result = _context3.sent;
            return _context3.abrupt("break", 24);

          case 24:
            console.log(JSON.stringify(result));
            res.status(200).json(result);

          case 26:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function (_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}());
/*
router.get('/menus', async (req, res) => {
  const { tipo } = req.body;
  const db = await connectMenu();
  var result;
  console.log("Typo de Menu 1: "+tipo);
  if(tipo=="rol"){
      console.log("1");
    result = await db.collection('menu').find({"tipo":"rol"}).toArray();
  } else {
    console.log("2");
    result = await db.collection('menu').find({"tipo":"estatus"}).toArray();
  }
  console.log(result);
  res.status(200).json(result);
});
*/

router.get('/:tipo', /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    var tipo, db, result;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            console.info("Get menus start....");
            tipo = req.params.tipo;
            _context4.next = 4;
            return (0, _database.connectMenu)();

          case 4:
            db = _context4.sent;
            console.log("Typo de Menu: " + tipo);
            _context4.t0 = tipo;
            _context4.next = _context4.t0 === "rol" ? 9 : _context4.t0 === "estatus" ? 14 : 19;
            break;

          case 9:
            console.debug(tipo);
            _context4.next = 12;
            return db.collection('menu').find({
              "tipo": "rol"
            }).toArray();

          case 12:
            result = _context4.sent;
            return _context4.abrupt("break", 21);

          case 14:
            console.debug(tipo);
            _context4.next = 17;
            return db.collection('menu').find({
              "tipo": "estatus"
            }).toArray();

          case 17:
            result = _context4.sent;
            return _context4.abrupt("break", 21);

          case 19:
            result = {};
            res.send(204, result);

          case 21:
            console.info(" ....Get menus end");
            res.status(200).send(result);

          case 23:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function (_x7, _x8) {
    return _ref4.apply(this, arguments);
  };
}());
router.get('/account/:page', /*#__PURE__*/function () {
  var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var db, page, rowsSkip, result, registros;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return (0, _database.connectAccount)();

          case 2:
            db = _context5.sent;
            page = req.params.page;

            if (page == 1) {
              rowsSkip = 0;
            } else {
              rowsSkip = (page - 1) * limitPageSize;
            }

            console.log("Page: " + page + " Skip: " + rowsSkip);
            _context5.next = 8;
            return db.collection('cuentas').find({}).sort({
              _id: 1
            }).limit(limitPageSize).skip(rowsSkip).toArray();

          case 8:
            result = _context5.sent;
            _context5.next = 11;
            return db.collection('cuentas').countDocuments({});

          case 11:
            registros = _context5.sent;
            registros = registros - rowsSkip;

            if (registros <= limitPageSize) {
              registros = 0;
            }

            res.status(200).json({
              "table": result,
              "restante": registros
            });

          case 15:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function (_x9, _x10) {
    return _ref5.apply(this, arguments);
  };
}());
var _default = router;
exports["default"] = _default;