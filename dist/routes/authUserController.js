"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _database = require("../database");

var _email = require("../models/email");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var router = (0, _express.Router)();

var config = require('../config'); // Log In user, and validate if is ACTIVE or NOT


router.post('/logIn', /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res, next) {
    var db, _req$body, username, password, result;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return (0, _database.connectClient)();

          case 2:
            db = _context.sent;
            _req$body = req.body, username = _req$body.username, password = _req$body.password;
            _context.next = 6;
            return db.collection('usuarios').findOne({
              username: username
            });

          case 6:
            result = _context.sent;

            if (result) {
              _context.next = 12;
              break;
            }

            console.log("Usuario no valido");
            return _context.abrupt("return", res.status(404).send("Usuario no valido: " + result));

          case 12:
            if (!(password != result.password)) {
              _context.next = 17;
              break;
            }

            console.log("Contraseña no valido - INPUT: " + password + " BD: " + result.password);
            return _context.abrupt("return", res.status(404).send("Contraseña no valido"));

          case 17:
            if (!(result.estatus == "DISABLE")) {
              _context.next = 20;
              break;
            }

            console.log("Usuario desactivado");
            return _context.abrupt("return", res.status(403).send("Usuario sin permisos"));

          case 20:
            res.status(202).json({
              auth: true,
              rol: result.rol
            });

          case 21:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function (_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}()); //Create New User (Need Approve)

router.post('/signUp', /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var db, insertInput, existUser, result;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            console.log("Creando registro ....");
            _context2.next = 3;
            return (0, _database.connectClient)();

          case 3:
            db = _context2.sent;
            insertInput = {
              username: req.body.user,
              password: req.body.password,
              status: req.body.status,
              nameUser: req.body.userName,
              email: req.body.email
            };
            _context2.next = 7;
            return db.collection('usuarios').find({
              username: req.body.user
            }).count();

          case 7:
            existUser = _context2.sent;
            console.log(existUser);

            if (!(existUser > 0)) {
              _context2.next = 14;
              break;
            }

            console.log(".... Registro Existente");
            res.status(409).send("Usuario existente");
            _context2.next = 20;
            break;

          case 14:
            (0, _email.sendEmailRegistry)(insertInput);
            _context2.next = 17;
            return db.collection('usuarios').insertOne(insertInput);

          case 17:
            result = _context2.sent;
            console.log(".... Registro creado");
            res.status(201).json(insertInput);

          case 20:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function (_x4, _x5) {
    return _ref2.apply(this, arguments);
  };
}());
var _default = router;
exports["default"] = _default;