"use strict";

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var config = require('../config');

function verifyTk(req, res, next) {
  var token = req.headers['x-access-token'];

  if (!token) {
    return res.status(401).json({
      auth: false,
      message: 'Usuario no autorizado'
    });
  }

  var tokenDecode = _jsonwebtoken["default"].verify(token, config.secret);

  req.userId = tokenDecode;
  next();
}

module.exports = verifyTk;