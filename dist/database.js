"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.connectClient = connectClient;
exports.connectData = connectData;
exports.connectMenu = connectMenu;
exports.connectAccount = connectAccount;

var _mongodb = _interopRequireDefault(require("mongodb"));

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

require('dotenv').config();

var mongoURI = process.env.MONGODB_URI;
var usuariosBD = process.env.USUARIOS_DB;
var movimientosDB = process.env.MOVIMIENTOS_DB;
var menusBD = process.env.MENUS_DB;
var cuentasDB = process.env.CUENTAS_DB;

function connectClient() {
  return _connectClient.apply(this, arguments);
}

function _connectClient() {
  _connectClient = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    var client, db;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _mongodb["default"].connect(mongoURI, {
              useUnifiedTopology: true
            });

          case 3:
            client = _context.sent;
            db = client.db(usuariosBD);
            console.log('Success DB connect');
            return _context.abrupt("return", db);

          case 9:
            _context.prev = 9;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);

          case 12:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 9]]);
  }));
  return _connectClient.apply(this, arguments);
}

function connectData() {
  return _connectData.apply(this, arguments);
}

function _connectData() {
  _connectData = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
    var client, db;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return _mongodb["default"].connect(mongoURI, {
              useUnifiedTopology: true
            });

          case 3:
            client = _context2.sent;
            db = client.db(movimientosDB);
            console.log('Success DB connect');
            return _context2.abrupt("return", db);

          case 9:
            _context2.prev = 9;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 9]]);
  }));
  return _connectData.apply(this, arguments);
}

function connectMenu() {
  return _connectMenu.apply(this, arguments);
}

function _connectMenu() {
  _connectMenu = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
    var client, db;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _context3.next = 3;
            return _mongodb["default"].connect(mongoURI, {
              useUnifiedTopology: true
            });

          case 3:
            client = _context3.sent;
            db = client.db(menusBD);
            console.log('Success DB connect');
            return _context3.abrupt("return", db);

          case 9:
            _context3.prev = 9;
            _context3.t0 = _context3["catch"](0);
            console.log(_context3.t0);

          case 12:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 9]]);
  }));
  return _connectMenu.apply(this, arguments);
}

function connectAccount() {
  return _connectAccount.apply(this, arguments);
}

function _connectAccount() {
  _connectAccount = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
    var client, db;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            _context4.next = 3;
            return _mongodb["default"].connect(mongoURI, {
              useUnifiedTopology: true
            });

          case 3:
            client = _context4.sent;
            db = client.db(cuentasDB);
            console.log('Success DB connect');
            return _context4.abrupt("return", db);

          case 9:
            _context4.prev = 9;
            _context4.t0 = _context4["catch"](0);
            console.log(_context4.t0);

          case 12:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 9]]);
  }));
  return _connectAccount.apply(this, arguments);
}